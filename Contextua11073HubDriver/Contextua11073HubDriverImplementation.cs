﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Events.UserServiceEvents;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;
using caalhp.IcePluginAdapters;
using System.IO;

using IEEE11073Parser;
using System.Threading.Tasks;

namespace Contextua11073HubDriver
{
    public class Contextua11073HubDeviceDriverImplementation : IDeviceDriverCAALHPContract
    {
        private IDeviceDriverHostCAALHPContract _host;
        private int _processId;
        private readonly string _server;
        private readonly string _port;
        private readonly IEEE11073ParserFacade _facade;

        string _cpr;
        string _passkey;
       
        
        public Contextua11073HubDeviceDriverImplementation()
        {
            _server = ConfigurationManager.AppSettings.Get("IEEEServer");
            _port = ConfigurationManager.AppSettings.Get("IEEEPort");
            _cpr = ConfigurationManager.AppSettings.Get("_cpr");
            _passkey = ConfigurationManager.AppSettings.Get("_passkey");
            _facade = new IEEE11073ParserFacade();

           //string path = Assembly.GetExecutingAssembly().
            var path = AssemblyDirectory;

            ConnectToHub();
            
        }

        private void ConnectToHub()
        {   
            Console.WriteLine("Pre setting event handler");

            _facade.NewIEEE11073Measurement += _facade_NewIEEE11073Measurement;

            //_facade.StartIEEE11073Parser(new[] { _server, _port });
            //_facade.StartIEEE11073Parser(null);
            //var task = Task.Run(()=>{ _facade.StartIEEE11073Parser(new[] { "192.168.0.2", "9005" }); });
            var task = Task.Run(() =>
            {
                try
                {
                    _facade.StartIEEE11073Parser(new[] { _server, _port });
                    //_facade.StartIEEE11073Parser(new string[] { }); //With automatic configuration (BonJour)
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed starting IEEE 11073 Parser - with message: " + e.Message);
                }
            });


            task.Wait();

        }

        public double GetMeasurement()
        {
            //var rnd = new System.Random();
            return 0.0;
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            //throw new System.NotImplementedException();
        }

        public string GetName()
        {
            return "Contextua11073HubDriver";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
        {

            _host = hostObj;
            _processId = processId;
            //var initEvent = new InitializingEvent
            //{
            //    CallerName = GetName(),
            //    CallerProcessId = _processId,
            //    Name = GetName()
            //};
            //var preInit = EventHelper.CreateEvent(SerializationType.Json, initEvent);
            ////_host.Host.ReportEvent(EventHelper.CreateEvent(_processId, GetName(), "Initializing " + GetName()));
            //_host.Host.ReportEvent(preInit);
            //var initializedEvent = new InitializedEvent()
            //{
            //    CallerName = GetName(),
            //    CallerProcessId = _processId,
            //    Name = GetName()
            //};
            //var postInit = EventHelper.CreateEvent(SerializationType.Json, initializedEvent);
            ////_host.Host.ReportEvent(EventHelper.CreateEvent(_processId, GetName(), "Started IEEE11073Parser using " + _server + ":" + _port));
            //_host.Host.ReportEvent(postInit);
        }

        private void _facade_NewIEEE11073Measurement(object sender, NewIEEE11073MeasurementArgs e)
        {
            Console.WriteLine("_facade_NewIEEE11073Measurement called");
            ParseAndReportResult(e.data);
        }

        private void ParseAndReportResult(BasicParseResult data)
        {
            Console.WriteLine("Recieved: " + data.Value + " - and sending ");

            switch (data.Type)
            {
                case ParseResultType.BloodPressure:
                    {
                        var res = data as BloodPressureParseResult;
                        if (res != null)
                        {
                            var theEvent = new BloodPressureMeasurementEvent
                            {
                                CallerName = GetName(),
                                CallerProcessId = _processId,
                                Diastolic = res.DiastolicValue,
                                Systolic = res.SystolicValue

                            };
                            var measurementEvent = EventHelper.CreateEvent(SerializationType.Json, theEvent);
                            _host.Host.ReportEvent(measurementEvent);
                        }
                    }
                    break;
                default:
                    {
                        var theEvent = new SimpleMeasurementEvent
                        {
                            CallerName = GetName(),
                            CallerProcessId = _processId,
                            MeasurementType = Enum.GetName(typeof (ParseResultType), data.Type),
                            Value = data.Value
                            
                        };
                        var measurementEvent = EventHelper.CreateEvent(SerializationType.Json, theEvent);
                        _host.Host.ReportEvent(measurementEvent);
                    }
                    break;
            }
        }
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        } 
}
   
}