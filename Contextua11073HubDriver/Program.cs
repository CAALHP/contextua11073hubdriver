﻿using Contextua11073HubDriver;
using caalhp.IcePluginAdapters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contextua11073HubDriverICE
{
    public class Program
    {
        public static void Main(string[] args)
        {
            

            try
            {
                //Debugger.Break();
                var imp = new Contextua11073HubDeviceDriverImplementation();
                var adapter = new DeviceDriverAdapter("localhost", imp);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
            }

            Console.ReadLine();
        }
    }
}
